"""
This guardian uses awg to inject some lines only when we get to nominal low noise.
It will detect when the guardian state drops out of nominal low noise and stop the injections.
It will restart them when it gets back up to low noise.

Add new lines in the start function below.

If you happen to RELOAD this guardian while in the INJECTING state, the awgs dictionary will be dumped and we will lose connection to the testpoints.
Please DO NOT reload in the INJECTING state.  Instead, request IDLE first, then reload, then re-request the INJECTING state.
If you forget, that is okay: just request IDLE then INJECTING again, this will end and restart the injections and repopulate the awgs dictionary.

Dan Brown, Craig Cahillane, March 22 2019
"""

from guardian import GuardState
import time
import awg
import awgbase
import atexit
import sys

request = 'IDLE'
nominal = 'IDLE'

awgs = {}

class SineMultiple(awg.Excitation):
    "Class so awg can inject multiple sine wave excitations on a single channel."
    def __init__(self, chan, ampl=[0], freq=[0], phase=[0], offset=[0], start=0,
                 duration=-1, restart=-1):
        """
        Initialize a `Sine` excitation. example

            a = SineMultiple("H1:SQZ-LO_SERVO_EXC_EXC", [1, 1], [100, 1000], [0, 0], [0, 0])
            a.start()
            a.stop()

        :Parameters:
          chan : string
            Name of the excitation channel to be driven.
          ampl : float
            Amplitude in counts.
          freq : float, list
            Frequency in cycles per sec.
          phase : float, list
            Phase in radians.
          offset : float, list
            Offset in counts.
          start : float
            Start time in GPS sec (immediate if = 0).
          duration : float
            Duration in sec (infinite if < 0).
          restart : float
            Restart time in sec (no restart if <= 0).

        """
        import copy
        awg.Excitation.__init__(self, chan, start=start, duration=duration, restart=restart)

        assert(len(ampl) == len(freq) == len(phase) == len(offset))

        for i, vals in enumerate(zip(ampl, freq, phase, offset)):
            if i == 0:
                c = self.components[0]
                c.wtype = awgbase.awgSine
                c.par = vals
            else:
                c = copy.deepcopy(self.comp)
                c.wtype = awgbase.awgSine
                c.par = vals
                self.components.append(c)

##############################

class INIT(GuardState):
    index = 0
    def main(self):
        return 'IDLE'


class IDLE(GuardState):
    """Idle state for AWG_LINES to sit in most of the time
    """
    index = 2
    def main(self):
        log(awgs)
    def run(self):
        return True

class SET_UP_INJECTIONS(GuardState):
    """Set up awg injections.
    CHANGE AWG LINES/FILTERS HERE!
    Also make any ezca excitation-on type switches here.
    """
    index = 5
    request = False
    def main(self):
        #cleanup is needed after model restart, do everytime to ensure there is no stale test points.
        log("Performing awg_cleanup to clear any old test points.")
        awg.awg_cleanup()
        # Set up awg injections
        #awgs['9MHz_intensity_72.3Hz'] = SineMultiple('H1:LSC-EXTRA_AO_3_EXC', [2e-2], [72.3], [0], [0])
        awgs['intensity_47.3Hz_222Hz_4222Hz'] = SineMultiple(
            'H1:PSL-ISS_SECONDLOOP_EXCITATION_EXC', 
            [10000, 1500, 500],
            [47.3, 222, 4222], 
            [0,0,0], 
            [0,0,0]
        )
        awgs['frequency_25.2Hz_201Hz_4500Hz'] = SineMultiple('H1:LSC-EXTRA_AO_2_EXC', [1, 0.3, 0.007], [25.2, 203, 4500], [0,0,0], [0,0,0])
        awgs['prcl_35.3Hz'] = SineMultiple('H1:LSC-PRCL1_EXC', [0.005], [35.3], [0])
        awgs['srcl_38.1Hz'] = SineMultiple('H1:LSC-SRCL1_EXC', [0.004], [38.1], [0]) 
        awgs['mich_40.7Hz'] = SineMultiple('H1:LSC-MICH1_EXC', [0.015], [40.7], [0])                
        #awgs['intensity_3.25kHz'] = SineMultiple('H1:PSL-ISS_SECONDLOOP_EXCITATION_EXC', [12000, 1200], [76.4, 3250], [0, 0], [0, 0]) #GLM commented out for testing

        # jitter coupling lines - https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=68036
        awgs['jitter_111.3Hz_167.1Hz_387.4Hz'] = SineMultiple(
          'H1:IMC-PZT_YAW_EXC',
          [8, 8, 8],
          [111.3, 167.1, 387.4],
          [0, 0, 0],
          [0, 0, 0]
        )

        # Enable the excitation switches
        #ezca['LSC-OUTPUT_MTRX_11_9'] = 1
        #ezca['LSC-MOD_RF9_AM_EXCITATIONEN'] = 1
        ezca['PSL-ISS_THIRDLOOP_POLE'] = 0
        ezca['LSC-REFL_SERVO_COMEXCEN'] = 1
        ezca['PSL-ISS_SECONDLOOP_EXCITATION_GAIN'] = 1 

        log(awgs)
        return True
        
class SET_UP_ASC_INJECTIONS(GuardState):
    """Set up awg injections.
    CHANGE AWG LINES/FILTERS HERE!
    Also make any ezca excitation-on type switches here.
    """
    index = 6
    request = False
    def main(self):
        # set up ASC notches for 8 Hz injections
        ezca.get_LIGOFilter('ASC-INP1_P').switch_on('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-INP1_Y').switch_on('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-MICH_P').switch_on('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-MICH_Y').switch_on('FM6', wait=False)
        time.sleep(1)
        ezca.get_LIGOFilter('ASC-PRC2_P').switch_on('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-PRC2_Y').switch_on('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-SRC2_P').switch_on('FM8', wait=False)
        ezca.get_LIGOFilter('ASC-SRC2_Y').switch_on('FM8', wait=False)
        ezca.get_LIGOFilter('ASC-SRC1_P').switch_on('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-SRC1_Y').switch_on('FM6', wait=False)
        time.sleep(1)
        ezca.get_LIGOFilter('ASC-DHARD_P').switch_on('FM9', wait=False)
        ezca.get_LIGOFilter('ASC-DHARD_Y').switch_on('FM10', wait=False)
        ezca.get_LIGOFilter('ASC-CHARD_P').switch_on('FM7', wait=False)
        ezca.get_LIGOFilter('ASC-CHARD_Y').switch_on('FM7', wait=False)
        time.sleep(1)
        ezca.get_LIGOFilter('ASC-DSOFT_P').switch_on('FM4', wait=False)
        ezca.get_LIGOFilter('ASC-DSOFT_Y').switch_on('FM4', wait=False)
        ezca.get_LIGOFilter('ASC-CSOFT_P').switch_on('FM4', wait=False)
        ezca.get_LIGOFilter('ASC-CSOFT_Y').switch_on('FM4', wait=False)
        # Set up awg injections
        awgs['CHARD_Y_8Hz'] = SineMultiple('H1:ASC-CHARD_Y_SM_EXC', [3000], [8.125], [0])               


        log(awgs)
        return True

class INJECTING(GuardState):
    """Injecting lines defined in awgs dictionary at the top of this file.
    """
    index = 10
    def main(self):
        # Loop through the awg injections and start them all
        for name, awg in awgs.items():
            log("Starting " + name)
            awg.start(ramptime=5)

        log(awgs)
        return True

    def run(self):
        return True

class STOP_INJECTING(GuardState):
    """Stop injecting lines, and clear testpoints.
    Make sure to undo the ezca excitation-on switches.
    """
    index = 15
    request = False
    def main(self):
        # Stop injections
        for name, awg in awgs.items():
            log("Stopping " + name)
            if awg.started:
                awg.stop(ramptime=5)

        # Clear injections
        for name, awg in awgs.items():
            log("Clearing " + name)
            awg.clear()

        # Disengage excitation on switches
        ezca['LSC-OUTPUT_MTRX_11_9'] = 0
        ezca['LSC-MOD_RF9_AM_EXCITATIONEN'] = 0
        ezca['LSC-REFL_SERVO_COMEXCEN'] = 0
        ezca['PSL-ISS_SECONDLOOP_EXCITATION_GAIN'] = 0
        ezca['PSL-ISS_THIRDLOOP_POLE'] = 1
        
        # turn off 8 Hz notches
        ezca.get_LIGOFilter('ASC-DHARD_P').switch_off('FM9', wait=False)
        ezca.get_LIGOFilter('ASC-DHARD_Y').switch_off('FM10', wait=False)
        ezca.get_LIGOFilter('ASC-CHARD_P').switch_off('FM7', wait=False)
        ezca.get_LIGOFilter('ASC-CHARD_Y').switch_off('FM7', wait=False)
        time.sleep(1)
        ezca.get_LIGOFilter('ASC-DSOFT_P').switch_off('FM4', wait=False)
        ezca.get_LIGOFilter('ASC-DSOFT_Y').switch_off('FM4', wait=False)
        ezca.get_LIGOFilter('ASC-CSOFT_P').switch_off('FM4', wait=False)
        ezca.get_LIGOFilter('ASC-CSOFT_Y').switch_off('FM4', wait=False)
        time.sleep(1)
        ezca.get_LIGOFilter('ASC-INP1_P').switch_off('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-INP1_Y').switch_off('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-MICH_P').switch_off('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-MICH_Y').switch_off('FM6', wait=False)
        time.sleep(1)
        ezca.get_LIGOFilter('ASC-PRC2_P').switch_off('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-PRC2_Y').switch_off('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-SRC2_P').switch_off('FM8', wait=False)
        ezca.get_LIGOFilter('ASC-SRC2_Y').switch_off('FM8', wait=False)
        ezca.get_LIGOFilter('ASC-SRC1_P').switch_off('FM6', wait=False)
        ezca.get_LIGOFilter('ASC-SRC1_Y').switch_off('FM6', wait=False)

        log(awgs)

        return True

edges = [
    ('INIT', 'IDLE'),
    ('IDLE', 'SET_UP_INJECTIONS'),
    ('SET_UP_INJECTIONS', 'INJECTING'),
    #('SET_UP_INJECTIONS', 'SET_UP_ASC_INJECTIONS'),
    #('SET_UP_ASC_INJECTIONS', 'INJECTING'),
    ('INJECTING', 'STOP_INJECTING'),
    ('STOP_INJECTING', 'IDLE'),

]
